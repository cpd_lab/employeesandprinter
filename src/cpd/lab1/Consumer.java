package cpd.lab1;

import static java.lang.Thread.sleep;

public class Consumer implements Runnable{
    private long delay;
    private Documents documents;

    public Consumer(long delay, Documents documents) {
        this.delay = delay;
        this.documents = documents;
    }

    @Override
    public void run() {
        try {
            int nrDocuments = 0;
            while (nrDocuments != -1){
                System.out.println("Printer waiting for documents");
                nrDocuments = documents.printDocument();
                sleep(delay);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
