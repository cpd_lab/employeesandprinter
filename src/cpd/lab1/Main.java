package cpd.lab1;

public class Main {

    public static void main(String[] args) {
	    Documents documents = new Documents();

        System.out.println("Init employees and printer");
        Producer producer = new Producer(documents);
        Consumer consumer = new Consumer(1000, documents);
        System.out.println("Start");
        new Thread(producer).start();
        new Thread(consumer).start();
    }
}
