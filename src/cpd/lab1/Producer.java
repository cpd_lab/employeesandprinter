package cpd.lab1;

import java.util.Random;

import static java.lang.Thread.sleep;

public class Producer implements Runnable{
    private int[] documentPackets = {1, 2, 3, 4, 5, 6, 7, 8, -1};
    private Documents documents;

    public Producer(Documents documents) {
        this.documents = documents;
    }

    @Override
    public void run() {
        try {
            for (int packet : documentPackets){
                Random rand = new Random();
                long delay = 1 + rand.nextInt(5);
                sleep(delay * 100);
                System.out.println("Employee sending document packet " + packet);
                documents.takeDocuments(packet);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
