package cpd.lab1;

public class Documents {
    private int documentsPacket;
    private int delay;
    private boolean printing = true;

    public synchronized void takeDocuments(int nrDocuments){        // produce
        while (printing){
            try {
                System.out.println("Employee is waiting");
                wait();
                System.out.println("Employee was notified");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Replace " + this.documentsPacket + " with " + nrDocuments);
        this.documentsPacket = nrDocuments;
        printing = true;
        System.out.println("Notify all threads");
        notifyAll();
    }

    public synchronized int printDocument(){                // consume
        while (!printing){
            try{
                System.out.println("Printer is waiting");
                wait();
                System.out.println("Printer was notified");
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }

        System.out.println("Print " + this.documentsPacket + " documents");
        System.out.println("Printer notify all employees");
        notifyAll();
        printing = false;
        return this.documentsPacket;
    }
}
